import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GlobalConstants} from '../../../@core/constant/GlobalConstants';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  constructor(private http: HttpClient) {
  }

  /**
   * 新增权限
   *
   * @param params
   */
  save(params: any): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysPermission/save', params);
  }

  /**
   * 修改权限
   *
   * @param params
   */
  update(params: any): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysPermission/update', params);
  }

  /**
   * 由ID获取权限信息
   *
   * @param id 权限ID
   */
  getById(id: string): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysPermission/getById', {params: {'id': id}});
  }

  /**
   * 获取权限列表
   *
   * @param params
   */
  list(params: any): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysPermission/listPage', {params});
  }

  /**
   * 获取所有权限列表
   */
  listAll(): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysPermission/listAll', {});
  }

  /**
   * 判断编码是否存在
   *
   * @param code 编码
   */
  checkCode(code: string): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysPermission/checkCode', {params: {'code': code}});
  }

  /**
   * 由ID删除权限信息
   *
   * @param id 权限ID
   */
  deleteById(id: string): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysPermission/deleteById', {}, {params: {'id': id}});
  }
}

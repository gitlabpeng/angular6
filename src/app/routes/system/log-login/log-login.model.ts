import { BaseModel } from '../../../@core/model/base.model';

/**
 * 登录日志model
 */
export class LogLoginModel extends BaseModel {

  /**
   * 账号
   */
  account: string;

  /**
   * 来源路径
   */
  requestPath: string;

  /**
   * 状态码
   */
  status: string;

  /**
   * 登录时间
   */
  loginTime: string;

  /**
   * 信息
   */
  message: string;

  /**
   * 国家
   */
  country: string;

  /**
   * 省份
   */
  province: string;

  /**
   * 城市
   */
  city: string;

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalConstants } from '../../../@core/constant/GlobalConstants';

@Injectable({
  providedIn: 'root'
})
export class DeptService {

  constructor(private http: HttpClient) {
  }

  /**
   * 新增部门
   *
   * @param params
   */
  save(params: any): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysDept/save', params);
  }

  /**
   * 修改部门
   *
   * @param params
   */
  update(params: any): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysDept/update', params);
  }

  /**
   * 由ID获取部门信息
   *
   * @param id 部门ID
   */
  getById(id: string): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysDept/getById', {params: {'id': id}});
  }

  /**
   * 获取部门列表
   *
   * @param params
   */
  list(params: any): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysDept/listPage', {params});
  }

  /**
   * 获取所有部门列表
   */
  listAll(): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysDept/listAll', {});
  }

  /**
   * 判断编码是否存在
   *
   * @param code 编码
   */
  checkCode(code: string): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysDept/checkCode', {params: {'code': code}});
  }

  /**
   * 由ID删除部门信息
   *
   * @param id 部门ID
   */
  deleteById(id: string): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysDept/deleteById', {}, {params: {'id': id}});
  }
}

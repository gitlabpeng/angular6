import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GlobalConstants } from '../../../@core/constant/GlobalConstants';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private http: HttpClient) {
  }

  /**
   * 新增角色
   *
   * @param params
   */
  save(params: any): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysRole/save', params);
  }

  /**
   * 修改角色
   *
   * @param params
   */
  update(params: any): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysRole/update', params);
  }

  /**
   * 由ID获取角色信息
   *
   * @param id 角色ID
   */
  getById(id: string): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysRole/getById', {params: {'id': id}});
  }

  /**
   * 由ID获取角色详细信息
   *
   * @param id 角色ID
   */
  getDetail(id: string): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysRole/getDetail', {params: {'id': id}});
  }

  /**
   * 角色授权
   *
   * @param params
   */
  authorize(params: any): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysRole/authorize', params);
  }

  /**
   * 获取角色列表
   *
   * @param params
   */
  list(params: any): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysRole/listPage', {params});
  }

  /**
   * 获取所有角色列表
   */
  listAll(): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysRole/listAll', {});
  }

  /**
   * 由实体类获取所有角色列表
   */
  listByModel(params: any): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysRole/listByModel', params);
  }

  /**
   * 判断编码是否存在
   *
   * @param code 编码
   */
  checkCode(code: string): Observable<any> {
    return this.http.get(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysRole/checkCode', {params: {'code': code}});
  }

  /**
   * 由ID删除角色信息
   *
   * @param id 角色ID
   */
  deleteById(id: string): Observable<any> {
    return this.http.post(GlobalConstants.getInstance().PERMISSION_SERVER + '/sysRole/deleteById', {}, {params: {'id': id}});
  }

}

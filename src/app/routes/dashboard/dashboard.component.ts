import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { applicationToSidebar, LayoutData } from '../../@layout/interface/layout-data';
import { LoginService } from '../pages/login/login.service';
import { LayoutService } from '../../@layout/@layout.service';
import { UserModel } from '../system/user/user.model';
import { ApplicationModel } from '../system/application/application.model';
import { GlobalConstants } from '../../@core/constant/GlobalConstants';
import { ResponseResultModel } from '../../@core/net/response-result.model';
import { setUserInfo } from '../../@core/util/user-info';
import EChartOption = echarts.EChartOption;
import { DashboardService } from './dashboard.service';
import { ChartModel } from '../../@core/model/chart.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  /**
   * 全局常量
   */
  global: GlobalConstants = GlobalConstants.getInstance();
  /**
   * 查询参数
   */
  params: any = {
    type: '0',
    timePoint: new Date()
  };
  /**
   * 用户拥有的应用服务（系统）
   */
  systems: ApplicationModel[] = [];
  /**
   * 颜色数组
   */
  colors: string[] = ['#f56a00', '#7265e6', '#ffbf00', '#00a2ae', '#fde3cf', '#ffa940', '#bae637', '#40a9ff', '#ffa39e', '#85a5ff'];
  /**
   * 访问量图表配置
   */
  visitsOption: any;
  /**
   * 访问来源分布图表配置
   */
  distributionOption: any;
  /**
   * 访问量加载中标识
   */
  visitsLoading: boolean;
  /**
   * 访问来源分布加载中标识
   */
  distributionLoading: boolean;

  constructor(private injector: Injector,
              private loginService: LoginService,
              private dashboardService: DashboardService,
              private layoutService: LayoutService) {
  }

  ngOnInit() {
    // 从服务器段获取到当前用户信息，更新布局数据
    this.loginService.getCurrentUser()
      .subscribe((res: ResponseResultModel) => {
        if (res.result) {
          const result: UserModel = res.result;
          if (result.applications) {
            result.applications.forEach((application: ApplicationModel) => {
              if (application.type === this.global.APPLICATION_SERVICE) {
                this.systems.push(application);
              }
            });
            const layoutData: LayoutData = {
              userBlock: {
                name: result.name,
                photo: result.photo,
                role: result.roles.map((role) => {
                  return role.name;
                }).join('，')
              },
              sidebarMenu: applicationToSidebar(result.applications, this.global.ROOT_PARENT_ID)
            };
            this.layoutService.setLayoutData(layoutData);
          }
          // 记录当前用户信息
          setUserInfo(result);
          // 初始化图表
          this.initVisits();
          this.initDistribution();
        }
      });
  }

  /**
   * 初始化访问量图表
   */
  initVisits() {
    this.visitsLoading = true;
    // 初始化日期管道对象
    const datePipe = new DatePipe('en-US');
    // 格式化Date类型
    this.params.timePoint = datePipe.transform(this.params.timePoint, 'yyyy-MM-dd HH:mm:ss');
    // 查询访问量
    this.dashboardService.getVisits(this.params)
      .subscribe((res: ResponseResultModel) => {
        if (res.result) {
          const result: ChartModel = res.result;
          this.visitsOption = {
            title: {
              text: result.name,
              x: 'center'
            },
            tooltip: {
              trigger: 'axis'
            },
            xAxis: {
              type: 'category',
              data: result.axisx
            },
            yAxis: {
              type: 'value'
            },
            series: [{
              name: '访问量',
              data: result.series[0].data,
              type: 'bar'
            }]
          };
        }
        this.visitsLoading = false;
      });
  }

  /**
   * 访问来源分布
   */
  initDistribution() {
    this.distributionLoading = true;
    // 查询访问来源分布
    this.dashboardService.getDistribution()
      .subscribe((res: ResponseResultModel) => {
        if (res.result) {
          const result: ChartModel = res.result;
          this.distributionOption = {
            title: {
              text: result.name,
              x: 'center'
            },
            tooltip: {
              trigger: 'item',
              formatter: '{a} <br/>{b} : {c} ({d}%)'
            },
            legend: {
              orient : 'vertical',
              x : 'left',
              data: result.axisx
            },
            series: [{
              name: '访问来源',
              type: 'pie',
              radius : '55%',
              center: ['50%', '60%'],
              data: result.series[0].data
            }]
          };
        }
        this.distributionLoading = false;
      });
  }

  /**
   * 时间维度变更监听
   *
   * @param result
   */
  onTypeChange(result: any): void {
    this.initVisits();
  }

  /**
   * 时间日期变更监听
   *
   * @param result
   */
  onDateChange(result: Date): void {
    this.initVisits();
  }

  /**
   * 跳转
   *
   * @param id 目标应用ID
   * @param url 目标路径
   */
  private goTo(id: string, url: string) {
    setTimeout(() => {
      this.injector.get(Router).navigateByUrl(this.global.APP_PREFIX + url);
    });
  }

}

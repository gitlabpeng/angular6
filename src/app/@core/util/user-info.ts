import { UserModel } from '../../routes/system/user/user.model';
import { GlobalConstants } from '../constant/GlobalConstants';

/**
 * 获取localStorage中的用户信息
 */
export function getUserInfo(): UserModel {
  return JSON.parse(localStorage.getItem(GlobalConstants.getInstance().CURRENT_USER));
}

/**
 * 记录当前用户信息到localStorage
 *
 * @param user
 */
export function setUserInfo(user: UserModel): void {
  localStorage.setItem(GlobalConstants.getInstance().CURRENT_USER, JSON.stringify(user));
}

/**
 * 删除localStorage中的用户信息
 */
export function removeUserInfo(): void {
  localStorage.removeItem(GlobalConstants.getInstance().CURRENT_USER);
}
